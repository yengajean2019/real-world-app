# Overview

This repository contains tests for a real world ecommerce app
:realworld.com

# Test plan

The test plan can be found at the following link

[Test plan](https://docs.google.com/document/d/1CjC5T2sHiTl1_OecVQ12-3KMUpSy8g93/edit?usp=sharing&ouid=114473922582071672310&rtpof=true&sd=true)

# Prerequisites

The only requirement for this project is to have node.js

# Installation

```bash
npm install
```
# Start cypress

```bash
npx cypress:open
```

## TESTS

| Type | Location                                  |
| ---- | ----------------------------------------  |
| api  | [cypress/tests/api](./cypress/tests/api)  |
| ui   | [cypress/tests/ui](./cypress/tests/ui)    |
| unit | [cypress/tests/unit](./cypress/tests/UNIT)|

# Reporting

- Link
